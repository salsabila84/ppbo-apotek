/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author toshiba
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controller.Koneksi;

public class MdlObat {

private JPanel contentPane;
    private JTextField txtKode;
    private JTextField txtNama;
    private JTextField txtHarga;
    private JTextField txtJumlah;

    public void simpanobat(String Kode, String Nama, String Harga, String Jumlah) {
        java.sql.Connection con;
        con = new Koneksi().connect();
        String sql = "insert obat values(?,?,?,?)";
        PreparedStatement pstmt;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, Kode);
            pstmt.setString(2, Nama);
            pstmt.setString(3, Harga);
            pstmt.setString(4, Jumlah);
            pstmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Berhasil menyimpan", "sukses", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            JOptionPane.showMessageDialog(null, "Gagal Menyimpan", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void hapusobat(String Kode) {
        java.sql.Connection con;
        con = new Koneksi().connect();
        String sql = "delete from obat where id_obat= ?";
        PreparedStatement pstmt;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, Kode);
            pstmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Terhapus", "sukses", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void updateobat(String Kode, String Nama, String Harga, String Jumlah) {
        java.sql.Connection con;
        con = new Koneksi().connect();
        String sql = "update obat set nama= ?, harga= ?, stok= ? where id_obat= ?";
        PreparedStatement pstmt;
        try {
            pstmt = con.prepareStatement(sql);
            pstmt.setString(4, Kode);
            pstmt.setString(1, Nama);
            pstmt.setString(2, Harga);
            pstmt.setString(3, Jumlah);
            pstmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah", "sukses", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

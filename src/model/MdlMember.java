/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import controller.Koneksi;

public class MdlMember {

           
            public void simpanmember(String IDMember, String Nama, String Alamat, String NoHP) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "insert member values(?,?,?,?)";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDMember);
                            pstmt.setString(2, Nama);
                            pstmt.setString(3, Alamat);
                            pstmt.setString(4, NoHP);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Berhasil menyimpan", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            JOptionPane.showMessageDialog(null, "Gagal Menyimpan", "Peringatan", JOptionPane.WARNING_MESSAGE);
                    }
                }

            public void hapusmember(String IDMember) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql =  "delete from member where id_member= ?";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDMember);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Data Terhapus", "sukses", JOptionPane.INFORMATION_MESSAGE);                                             
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }
                }

            public void updatemember(String IDMember, String Nama, String Alamat, String NoHP) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "update member set nama= ?, alamat= ?, nohp= ? where id_member= ?";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(4, IDMember);
                            pstmt.setString(1, Nama);
                            pstmt.setString(2, Alamat);
                            pstmt.setString(3, NoHP);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }  
}
}

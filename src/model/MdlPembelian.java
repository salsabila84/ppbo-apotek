/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author toshiba
 */
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import controller.Koneksi;

public class MdlPembelian {

                public void simpanpembelian(String IDMember, String IDKaryawan, String Kode, String Jumlah, String Harga) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "insert pembelian values(?,?,?,?)";
                    PreparedStatement pstmt;
                    try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDMember);
                            pstmt.setString(2, IDKaryawan);
                            pstmt.setString(3, Kode);
                            pstmt.setString(4, Jumlah);
                            pstmt.setString(5, Harga);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Berhasil menyimpan", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            JOptionPane.showMessageDialog(null, "Gagal Menyimpan", "Peringatan", JOptionPane.WARNING_MESSAGE);
                    }
                }

                public void hapuspembelian(String IDMember) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql =  "delete from karyawan where IDMember= ?";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDMember);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Data Terhapus", "sukses", JOptionPane.INFORMATION_MESSAGE);                                              
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }
                }

                public void updatepembelian(String IDMember, String IDKaryawan, String Kode, String Jumlah, String Harga) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "UPDATE `pembelian` SET `id_member`='" + IDMember + "',`id_karyawan`='" + IDKaryawan + "',`kode`='" + Kode + "',`jumlah`='" + Jumlah + "',`harga`='" + Harga + "' WHERE `id_member`='" + IDMember + "' ";
                    Statement s;
                    try {
                            s = con.createStatement();
                            s.executeUpdate(sql);
                            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }
                }  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

 import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import controller.Koneksi;

public class MdlKaryawan {

           
            public void simpankaryawan(String IDKaryawan, String Nama,String User,String Pass, String Alamat, String NoHP) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "insert karyawan values(?,?,?,?,?,?)";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDKaryawan);
                            pstmt.setString(2, Nama);
                            pstmt.setString(3, User);
                            pstmt.setString(4, Pass);
                            pstmt.setString(5, Alamat);
                            pstmt.setString(6, NoHP);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Berhasil menyimpan", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            JOptionPane.showMessageDialog(null, "Gagal Menyimpan", "Peringatan", JOptionPane.WARNING_MESSAGE);
                    }
                }

            public void hapuskaryawan(String IDKaryawan) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql =  "delete from karyawan where id_karyawan= ?";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(1, IDKaryawan);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Data Terhapus", "sukses", JOptionPane.INFORMATION_MESSAGE);                                             
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }
                }

            public void updatekaryawan(String IDKaryawan, String Nama,String User,String Pass, String Alamat, String NoHP) {
                    java.sql.Connection con;
                    con = new Koneksi().connect();
                    String sql = "update karyawan set nama= ?, username= ?, password= ?, alamat= ?, noHP= ? where id_karyawan= ?";
                    PreparedStatement pstmt;
            try {
                            pstmt = con.prepareStatement(sql);
                            pstmt.setString(6, IDKaryawan);
                            pstmt.setString(1, Nama);
                            pstmt.setString(2, User);
                            pstmt.setString(3, Pass);
                            pstmt.setString(4, Alamat);
                            pstmt.setString(5, NoHP);
                            pstmt.executeUpdate();
                            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah", "sukses", JOptionPane.INFORMATION_MESSAGE);
                    } catch (SQLException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }  
}
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author toshiba
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Koneksi {

    private Connection koneksi = null;

    public Connection connect() {
        // untuk koneksi ke driver
        try {
            // try mencari library
            Class.forName("com.mysql.jdbc.Driver"); //LOAD DRIVER
            System.out.println("berhasil load driver");
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Tidak ada Driver " + cnfe);
        }

        // untuk koneksi ke database
        try {
            String url = "jdbc:mysql://localhost:3306/apotek"; //DRIVER MANA YG AKAN DIGUNAKAN
            koneksi = DriverManager.getConnection(url, "root", "");
            System.out.println("Berhasil koneksi");
                                    // JOptionPane.showMessageDialog(null,"Berhasil
            // koneksi","sukses",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException se) {
            System.out.println("Gagal koneksi " + se);
            JOptionPane.showMessageDialog(null, "Gagal Koneksi Database", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
        return koneksi;
    }
}

